# OpenHAB Secure GPIO

A secure* alternative to the OpenHAB remote GPIO.

_* featuring Mutual TLS encryption_

## Requirements

- A (probably any) Raspberry Pi
- An Adafruit [16-Channel 12-bit PWM/Servo Driver](https://www.adafruit.com/product/815). (For Hardware-PWM, Optional)

## Daemon Configuration

A schema is available here: [daemon/config.schema.json](./daemon/config.schema.json)

Example:
```yaml
# File path: ./config.yaml

# Listen IP; Optional, default is 0.0.0.0
ip: 192.168.1.2
# Listen port; Optional, default is 8443
port: 8443
# Optional, the first module in the list will be PWM module 0 and so on
pwm_modules:
    # For using the raspberry pi gpio for PWM
  - type: PiGpio
    # For an Adafruit PCA9685 PWM Driver
  - type: PCA9685
    address: 0
  - type: PCA9685
    # For a second, chained PCA9685 Driver
    address: 1
# Optional, the first module in the list will be analog input module 0 and so on
analog_inputs:
    # For using an MCP3002 IC. MCP3002, MCP3004 and MCP3208 are also supported with
    # the same configuration parameters
  - type: MCP3008
    # The SPI bus the IC is connected to, most PIs will have SPI0 and SPI1
    bus: 0
    # The chip select pin the IC is connected to, most PIs will have CE0, CE1 and
    # (only on SPI1) CE2
    chip_select: 0
```

## Bridge Configuration

To convert the certificates to the required base64 format you can use the following commands:
```bash
echo Server CA: && cat server-ca.pem | base64 -w 0 && echo
echo Client Certificate: && cat client-cert.pem | base64 -w 0 && echo
echo Client Key: && cat client-key.pem | base64 -w 0 && echo
```
