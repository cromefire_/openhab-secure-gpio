import aQute.bnd.gradle.Bundle

val grpcVersion: String by project

plugins {
    kotlin("jvm")
    id("com.github.johnrengelman.shadow") version "7.0.0"
    id("biz.aQute.bnd.builder") version "5.3.0"
}

group = "com.gitlab.cromefire.openhabsecuregpio"
version = rootProject.version
base.archivesBaseName = "secure-gpio"

repositories {
    mavenCentral()
    maven("https://openhab.jfrog.io/openhab/libs-release-local")
    maven("https://openhab.jfrog.io/openhab/libs-3rdparty-local") {
        content {
            includeGroup("org.eclipse.orbit.bundles")
        }
    }
}

dependencies {
    compileOnly("org.openhab.core.bom:org.openhab.core.bom.compile:3.1.0")
    compileOnly("org.openhab.core.bom:org.openhab.core.bom.openhab-core:3.1.0") {
        exclude("net.wimpi", "jamod")
    }
    testImplementation("org.openhab.core.bom:org.openhab.core.bom.test:3.1.0")
    implementation(project(":protocol"))
    runtimeOnly("io.grpc:grpc-netty:$grpcVersion")
}

java {
    targetCompatibility = JavaVersion.VERSION_11
    sourceCompatibility = JavaVersion.VERSION_11
}

tasks {
    val bundle by registering(Bundle::class) {
        with(convention.plugins["bundle"] as aQute.bnd.gradle.BundleTaskConvention) {
            group = "build"
            dependsOn(shadowJar)
            from(zipTree(shadowJar.get().outputs.files.singleFile))
            exclude("META-INF/versions/**/*")
            manifest {
                attributes["Import-Package"] = listOf(
                    "org.openhab.*",
                    "org.slf4j.*",
                ).joinToString(",")
                attributes["Bundle-Name"] = "Secure GPIO"
                attributes["Bundle-SymbolicName"] = "secure-gpio"
            }
            bnd(
                """
                    -exportcontents: com.gitlab.cromefire.openhabsecuregpio
                """.trimIndent()
            )
            archiveClassifier.set("bundle")
        }
    }

    compileKotlin {
        kotlinOptions {
            jvmTarget = "11"
        }
    }

    shadowJar {
        minimize {
            exclude(dependency("io.grpc:grpc-netty"))
        }
        archiveClassifier.set("shadow")
    }

    build.get().dependsOn(bundle)
}
