package com.gitlab.cromefire.openhabsecuregpio

import org.openhab.core.thing.ThingTypeUID
import org.openhab.core.thing.type.ChannelTypeUID

object SecureGpioConstants {
    const val bindingId = "secure-gpio"
    val thingTypeSecureGPIODevice = ThingTypeUID(bindingId, "gpio-remote")

    object Channels {
        val digitalInput = ChannelTypeUID(bindingId, "gpio-digital-input")
        val analogInput = ChannelTypeUID(bindingId, "gpio-analog-input")
        val analogThresholdInput = ChannelTypeUID(bindingId, "gpio-analog-threshold-input")
        val digitalOutput = ChannelTypeUID(bindingId, "gpio-digital-output")
        val pwmOutput = ChannelTypeUID(bindingId, "gpio-pwm-output")
    }
}
