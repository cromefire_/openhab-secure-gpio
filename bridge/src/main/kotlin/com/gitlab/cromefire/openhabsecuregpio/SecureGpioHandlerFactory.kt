package com.gitlab.cromefire.openhabsecuregpio

import com.gitlab.cromefire.openhabsecuregpio.handlers.SecureGpioRemoteHandler
import org.openhab.core.thing.Thing
import org.openhab.core.thing.ThingTypeUID
import org.openhab.core.thing.binding.BaseThingHandlerFactory
import org.openhab.core.thing.binding.ThingHandler
import org.openhab.core.thing.binding.ThingHandlerFactory
import org.osgi.service.component.annotations.Component


@Component(configurationPid = ["binding.gpio"], service = [ThingHandlerFactory::class])
open class SecureGpioHandlerFactory : BaseThingHandlerFactory() {
    override fun supportsThingType(thingTypeUID: ThingTypeUID): Boolean {
        return supportedThingTypesUids.contains(thingTypeUID)
    }

    override fun createHandler(thing: Thing): ThingHandler? {
        val thingTypeUID = thing.thingTypeUID

        if (SecureGpioConstants.thingTypeSecureGPIODevice == thingTypeUID) {
            return SecureGpioRemoteHandler(thing)
        }

        return null
    }

    companion object {
        private val supportedThingTypesUids: Collection<ThingTypeUID> =
            setOf(SecureGpioConstants.thingTypeSecureGPIODevice)
    }
}
