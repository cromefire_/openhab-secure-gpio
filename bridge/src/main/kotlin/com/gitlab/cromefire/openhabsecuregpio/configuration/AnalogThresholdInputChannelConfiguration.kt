package com.gitlab.cromefire.openhabsecuregpio.configuration

internal data class AnalogThresholdInputChannelConfiguration(
    var moduleId: Int? = null,
    var pin: Int? = null,
    var thresholdOn: Int? = null,
    var thresholdOff: Int? = null
)
