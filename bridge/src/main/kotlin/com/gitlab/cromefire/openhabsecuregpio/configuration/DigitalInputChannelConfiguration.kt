package com.gitlab.cromefire.openhabsecuregpio.configuration

internal data class DigitalInputChannelConfiguration(
    var gpioId: Int = -1,
    var invert: Boolean = false,
    var debouncingTime: Int = 10,
    var pullupdown: Int = 0
)
