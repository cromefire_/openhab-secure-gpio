package com.gitlab.cromefire.openhabsecuregpio.configuration

internal data class DigitalOutputChannelConfiguration(var gpioId: Int = -1, var invert: Boolean = false)
