package com.gitlab.cromefire.openhabsecuregpio.handlers

import com.gitlab.cromefire.openhabsecuregpio.protocol.SecureGpioGrpcKt
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import org.openhab.core.types.Command
import org.openhab.core.types.State
import org.slf4j.Logger

internal abstract class ChannelHandler<T>(
    protected val config: T,
    protected val client: SecureGpioGrpcKt.SecureGpioCoroutineStub,
    protected val scope: CoroutineScope,
    protected val updateState: (state: State) -> Unit
) {
    protected abstract val logger: Logger
    private var currentJob: Job

    init {
        this.validateParameters(config)
        currentJob = this.startConnection()
    }

    protected abstract fun validateParameters(config: T)
    protected abstract fun reset()
    protected abstract fun startConnection(): Job
    abstract suspend fun handleCommand(command: Command)
    fun reconnectIfNeeded() {
        if (currentJob.isCompleted) {
            logger.info("Reconnecting channel...")
            reset()
            currentJob = startConnection()
        }
    }
}
