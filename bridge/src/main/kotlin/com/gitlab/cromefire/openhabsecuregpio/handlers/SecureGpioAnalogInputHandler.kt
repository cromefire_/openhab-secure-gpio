package com.gitlab.cromefire.openhabsecuregpio.handlers

import com.gitlab.cromefire.openhabsecuregpio.configuration.AnalogInputChannelConfiguration
import com.gitlab.cromefire.openhabsecuregpio.protocol.SecureGpioGrpcKt
import com.gitlab.cromefire.openhabsecuregpio.protocol.readPinAnalogConfiguration
import io.grpc.Status
import io.grpc.StatusException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.openhab.core.library.types.DecimalType
import org.openhab.core.types.Command
import org.openhab.core.types.State
import org.slf4j.Logger
import org.slf4j.LoggerFactory

internal class SecureGpioAnalogInputHandler(
    config: AnalogInputChannelConfiguration,
    client: SecureGpioGrpcKt.SecureGpioCoroutineStub,
    scope: CoroutineScope,
    updateState: (state: State) -> Unit
) : ChannelHandler<AnalogInputChannelConfiguration>(config, client, scope, updateState) {
    override val logger: Logger = LoggerFactory.getLogger(this::class.java)

    override fun validateParameters(config: AnalogInputChannelConfiguration) {
        if (config.moduleId == null) {
            throw IllegalStateException("Module ID pin is unset")
        }
        if (config.pin == null) {
            throw IllegalStateException("Analog pin is unset")
        }
    }

    override fun reset() {}

    override fun startConnection(): Job {
        return scope.launch {
            try {
                client.readAnalogPin(readPinAnalogConfiguration {
                    module = config.moduleId!! + 1
                    pin = config.pin!! + 1
                    jitter = config.jitter
                })
                    .collect {
                        updateState(DecimalType(it.value.toLong()))
                    }
            } catch (e: StatusException) {
                if (e.status.code == Status.UNAVAILABLE.code) {
                    logger.info("Pin ${config.moduleId}:${config.pin} disconnected")
                    return@launch
                }
                logger.error("Unexpected status exception while sending message from channel", e)
            } catch (e: Throwable) {
                logger.error("Unknown exception while sending message from channel", e)
            }
        }
    }

    override suspend fun handleCommand(command: Command) {
        logger.debug("Input handlers don't handle commands")
    }
}
