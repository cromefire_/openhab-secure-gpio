package com.gitlab.cromefire.openhabsecuregpio.handlers

import com.gitlab.cromefire.openhabsecuregpio.configuration.DigitalOutputChannelConfiguration
import com.gitlab.cromefire.openhabsecuregpio.protocol.Gpio
import com.gitlab.cromefire.openhabsecuregpio.protocol.SecureGpioGrpcKt
import com.gitlab.cromefire.openhabsecuregpio.protocol.writeDigitalPin
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import org.openhab.core.library.types.OnOffType
import org.openhab.core.types.Command
import org.openhab.core.types.State
import org.slf4j.Logger
import org.slf4j.LoggerFactory

internal class SecureGpioDigitalOutputHandler(
    config: DigitalOutputChannelConfiguration,
    client: SecureGpioGrpcKt.SecureGpioCoroutineStub,
    scope: CoroutineScope,
    updateState: (state: State) -> Unit
) : ChannelHandler<DigitalOutputChannelConfiguration>(config, client, scope, updateState) {
    override val logger: Logger = LoggerFactory.getLogger(this::class.java)
    private var channel: Channel<Gpio.WriteDigitalPin> = Channel()

    override fun validateParameters(config: DigitalOutputChannelConfiguration) {
        require(config.gpioId >= 0) { "GPIO pin is unset" }
    }

    override fun reset() {
        channel.close()
        channel = Channel()
    }

    override fun startConnection(): Job {
        val job = scope.launch {
            try {
                client.writeDigitalPin(channel.consumeAsFlow())
            } catch (e: Throwable) {
                logger.error("Unknown exception while sending message from channel", e)
            }
        }
        scope.launch {
            try {
                logger.debug("Initializing pin ${config.gpioId}")
                channel.send(writeDigitalPin { pin = config.gpioId })
            } catch (e: Throwable) {
                logger.error("Unknown exception while initializing channel", e)
            }
        }

        return job
    }

    override suspend fun handleCommand(command: Command) {
        when (command) {
            is OnOffType -> {
                logger.debug("Setting pin ${config.gpioId} to $command")
                channel.send(writeDigitalPin {
                    value = when (command) {
                        OnOffType.ON -> true
                        OnOffType.OFF -> false
                    }
                })
                updateState(command)
            }
            else -> {
                logger.warn("Unknown command type ${command.javaClass}")
            }
        }
    }
}
