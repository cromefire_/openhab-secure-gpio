package com.gitlab.cromefire.openhabsecuregpio.handlers

import com.gitlab.cromefire.openhabsecuregpio.configuration.AnalogInputChannelConfiguration
import com.gitlab.cromefire.openhabsecuregpio.configuration.AnalogThresholdInputChannelConfiguration
import com.gitlab.cromefire.openhabsecuregpio.configuration.DigitalOutputChannelConfiguration
import com.gitlab.cromefire.openhabsecuregpio.configuration.DigitalInputChannelConfiguration
import com.gitlab.cromefire.openhabsecuregpio.configuration.PwmOutputChannelConfiguration
import com.gitlab.cromefire.openhabsecuregpio.configuration.RemoteConfiguration
import com.gitlab.cromefire.openhabsecuregpio.protocol.Gpio
import com.gitlab.cromefire.openhabsecuregpio.protocol.SecureGpioGrpcKt.SecureGpioCoroutineStub
import com.google.protobuf.Empty
import io.grpc.ConnectivityState
import io.grpc.Grpc
import io.grpc.ManagedChannel
import io.grpc.Status
import io.grpc.StatusException
import io.grpc.TlsChannelCredentials
import java.io.ByteArrayInputStream
import java.util.Base64
import java.util.Timer
import java.util.TimerTask
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLHandshakeException
import kotlin.concurrent.scheduleAtFixedRate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.openhab.core.thing.ChannelUID
import org.openhab.core.thing.Thing
import org.openhab.core.thing.ThingStatus
import org.openhab.core.thing.ThingStatusDetail
import org.openhab.core.thing.binding.BaseThingHandler
import org.openhab.core.types.Command
import org.slf4j.LoggerFactory
import com.gitlab.cromefire.openhabsecuregpio.SecureGpioConstants as Constants


class SecureGpioRemoteHandler(thing: Thing) : BaseThingHandler(thing) {
    private val logger = LoggerFactory.getLogger(SecureGpioRemoteHandler::class.java)
    private lateinit var client: SecureGpioCoroutineStub
    private var scope: CoroutineScope = CoroutineScope(Dispatchers.IO + SupervisorJob())
    private var managedChannel: ManagedChannel? = null
    private var timerTask: TimerTask? = null
    private val channelHandlers: MutableMap<ChannelUID, ChannelHandler<*>> = mutableMapOf()

    override fun initialize() {
        logger.info("Initializing Secure Gpio \"${thing.label}\"")
        scope.cancel()
        scope = CoroutineScope(Dispatchers.IO + SupervisorJob())
        scope.launch {
            try {
                val config: RemoteConfiguration = getConfigAs(RemoteConfiguration::class.java)
                val credentials =
                    try {
                        val serverCa = Base64.getDecoder().decode(config.serverCa)
                        val clientCert = Base64.getDecoder().decode(config.clientCert)
                        val clientKey = Base64.getDecoder().decode(config.clientKey)
                        @Suppress("BlockingMethodInNonBlockingContext")
                        TlsChannelCredentials.newBuilder()
                            .keyManager(
                                ByteArrayInputStream(clientCert),
                                ByteArrayInputStream(clientKey)
                            )
                            .trustManager(ByteArrayInputStream(serverCa))
                            .build()
                    } catch (e: IllegalArgumentException) {
                        logger.error("Initialization error", e)
                        updateStatus(
                            ThingStatus.OFFLINE,
                            ThingStatusDetail.CONFIGURATION_ERROR,
                            "Invalid base64 characters in certificate configuration: ${e.message}"
                        )
                        return@launch
                    }
                val managedChannel =
                    Grpc.newChannelBuilderForAddress(config.host, config.port, credentials)
                        .enableRetry()
                        .build()
                this@SecureGpioRemoteHandler.managedChannel = managedChannel
                client = SecureGpioCoroutineStub(managedChannel)
                logger.info("Fetching daemon info")
                val info: Gpio.DaemonInfo
                try {
                    info = client.withDeadlineAfter(3, TimeUnit.SECONDS).getInfo(Empty.getDefaultInstance())
                } catch (e: StatusException) {
                    val cause = e.cause
                    if (cause is SSLHandshakeException) {
                        updateStatus(
                            ThingStatus.OFFLINE,
                            ThingStatusDetail.COMMUNICATION_ERROR,
                            "Connection security could not be verified (invalid SSL certificates)"
                        )
                    } else if (e.status.code in listOf(Status.UNAVAILABLE.code, Status.DEADLINE_EXCEEDED.code)) {
                        updateStatus(
                            ThingStatus.OFFLINE,
                            ThingStatusDetail.COMMUNICATION_ERROR,
                            "Device unavailable (${e.status.description})"
                        )
                        logger.warn("Device unavailable", e)
                        return@launch
                    }
                    logger.error(e.status.code.value().toString())
                    logger.error("Error while fetching the info from ${config.host}:${config.port}", e)
                    updateStatus(
                        ThingStatus.OFFLINE,
                        ThingStatusDetail.COMMUNICATION_ERROR,
                        "Error fetching status: ${e.status.description} (${e.status.code})"
                    )
                    return@launch
                }
                updateStatus(
                    ThingStatus.ONLINE,
                    ThingStatusDetail.NONE,
                    "Connected to ${info.hostname} (daemon v${info.version} running on ${info.hardwareModel})"
                )
                logger.info("Connecting to channels")
                thing.channels.forEach {
                    when (it.channelTypeUID) {
                        Constants.Channels.digitalInput -> {
                            val channelConfig: DigitalInputChannelConfiguration =
                                it.configuration.`as`(DigitalInputChannelConfiguration::class.java)
                            channelHandlers[it.uid] =
                                SecureGpioDigitalInputHandler(channelConfig, client, scope) { state ->
                                    updateState(it.uid, state)
                                }
                        }
                        Constants.Channels.analogInput -> {
                            val channelConfig: AnalogInputChannelConfiguration =
                                it.configuration.`as`(AnalogInputChannelConfiguration::class.java)
                            channelHandlers[it.uid] =
                                SecureGpioAnalogInputHandler(channelConfig, client, scope) { state ->
                                    updateState(it.uid, state)
                                }
                        }
                        Constants.Channels.analogThresholdInput -> {
                            val channelConfig: AnalogThresholdInputChannelConfiguration =
                                it.configuration.`as`(AnalogThresholdInputChannelConfiguration::class.java)
                            channelHandlers[it.uid] =
                                SecureGpioAnalogThresholdInputHandler(channelConfig, client, scope) { state ->
                                    updateState(it.uid, state)
                                }
                        }
                        Constants.Channels.digitalOutput -> {
                            val channelConfig: DigitalOutputChannelConfiguration =
                                it.configuration.`as`(DigitalOutputChannelConfiguration::class.java)
                            channelHandlers[it.uid] =
                                SecureGpioDigitalOutputHandler(channelConfig, client, scope) { state ->
                                    updateState(it.uid, state)
                                }
                        }
                        Constants.Channels.pwmOutput -> {
                            val channelConfig: PwmOutputChannelConfiguration =
                                it.configuration.`as`(PwmOutputChannelConfiguration::class.java)
                            channelHandlers[it.uid] =
                                SecureGpioPwmOutputHandler(channelConfig, client, scope) { state ->
                                    updateState(it.uid, state)
                                }
                        }
                    }
                }
                managedChannel.notifyWhenStateChanged(ConnectivityState.TRANSIENT_FAILURE) {
                    if (managedChannel.getState(true) == ConnectivityState.TRANSIENT_FAILURE) {
                        updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.COMMUNICATION_ERROR, "Transient failure")
                    }
                }
            } catch (e: Throwable) {
                logger.error("Initialization error", e)
                updateStatus(
                    ThingStatus.OFFLINE,
                    ThingStatusDetail.HANDLER_INITIALIZING_ERROR,
                    "Unknown failure: ${e.javaClass.simpleName}: ${e.message}"
                )
                return@launch
            }
            val timer = Timer("Secure GPIO ${thing.thingTypeUID.id}", true)
            timerTask = timer.scheduleAtFixedRate(10_000, 10_000) {
                runBlocking {
                    val info: Gpio.DaemonInfo
                    try {
                        info = client.getInfo(Empty.getDefaultInstance())
                    } catch (e: StatusException) {
                        if (e.status.code in listOf(Status.UNAVAILABLE.code, Status.DEADLINE_EXCEEDED.code)) {
                            updateStatus(
                                ThingStatus.OFFLINE,
                                ThingStatusDetail.COMMUNICATION_ERROR,
                                "Device unavailable (${e.status.code})"
                            )
                            logger.warn("Device unavailable", e)
                        } else {
                            logger.error("Error while polling (${thing.thingTypeUID.id})", e)
                            updateStatus(
                                ThingStatus.OFFLINE,
                                ThingStatusDetail.COMMUNICATION_ERROR,
                                "Error fetching status: ${e.status.description} (${e.status.code})"
                            )
                        }
                        return@runBlocking
                    }
                    updateStatus(
                        ThingStatus.ONLINE,
                        ThingStatusDetail.NONE,
                        "Connected to ${info.hostname} (daemon v${info.version} running on ${info.hardwareModel})"
                    )
                    for (handler in channelHandlers.values) {
                        handler.reconnectIfNeeded()
                    }
                }
            }
        }
    }

    override fun handleCommand(channelUID: ChannelUID, command: Command) {
        logger.debug("Handling command $command for $channelUID")
        scope.launch {
            try {
                channelHandlers[channelUID]?.handleCommand(command) ?: logger.warn("No handler for $channelUID")
            } catch (e: Throwable) {
                logger.error("Unknown error while handling command", e)
            }
        }
    }

    override fun dispose() {
        managedChannel?.shutdownNow()?.awaitTermination(10, TimeUnit.SECONDS)
        timerTask?.cancel()
        scope.cancel()
    }
}
