import java.io.ByteArrayOutputStream
import java.nio.charset.Charset
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.listProperty
import org.gradle.kotlin.dsl.mapProperty
import org.gradle.kotlin.dsl.property

@Suppress("LeakingThis")
open class SetupRust : DefaultTask() {
    private val defaultHost by lazy {
        val out = ByteArrayOutputStream()
        project.exec {
            commandLine("rustup", "show")
            standardOutput = out
        }
        for (line in out.toString(Charset.forName("UTF-8")).split('\n')) {
            if (line.startsWith("Default host: ")) {
                return@lazy line.substring(14)
            }
        }
    }

    @Input
    val rustVersion = project.objects.property<String>().convention("stable")

    @Input
    val targets = project.objects.listProperty<String>().convention(mutableListOf())

    init {
        outputs.upToDateWhen {
            toolchainConfigured()
        }
        onlyIf {
            // in case offline mode is enabled don't try to download if
            if (project.gradle.startParameter.isOffline) {
                if (toolchainConfigured()) {
                    project.logger.info("Skipping rust setup in offline mode.")
                } else {
                    throw IllegalStateException("Unable to setup rust in offline mode.")
                }
                return@onlyIf false
            }
            true
        }
        group = "rust"
    }

    private fun toolchainConfigured(): Boolean {
        val out = ByteArrayOutputStream()
        project.exec {
            commandLine("rustup", "toolchain", "list")
            standardOutput = out
        }
        for (toolchain in out.toString(Charset.forName("UTF-8")).split('\n')) {
            if (toolchain == "${rustVersion.get()}-$defaultHost (override)") {
                return true
            }
        }
        return false
    }

    @TaskAction
    fun execute() {
        rustVersion.finalizeValue()
        targets.finalizeValue()

        val installCmd = mutableListOf(
            "rustup",
            "toolchain",
            "install",
            "--no-self-update",
            "--profile",
            "minimal",
            "${rustVersion.get()}-$defaultHost"
        )

        if (targets.get().isNotEmpty()) {
            installCmd.add("--target")
            installCmd.add(targets.get().joinToString(","))
        }

        project.exec {
            commandLine(installCmd)
        }

        project.exec {
            commandLine("rustup", "override", "set", "${rustVersion.get()}-$defaultHost")
        }
    }
}

open class CargoTask : DefaultTask() {
    @Input
    val args = project.objects.listProperty<String>()
    @Input
    val environment = project.objects.mapProperty<String, String?>()

    @TaskAction
    fun execute() {
        args.finalizeValue()
        environment.finalizeValue()

        project.exec {
            commandLine("cargo")
            environment(this@CargoTask.environment.get())
            args(this@CargoTask.args.get())
        }
    }
}
