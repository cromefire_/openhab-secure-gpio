version = rootProject.version

configurations {
    create("protos") {
        isCanBeConsumed = false
        isCanBeResolved = true
    }
}

dependencies {
    "protos"(
        project(
            mapOf(
                "path" to ":protocol", "configuration" to "protos"
            )
        )
    )
}

tasks {
    val protoSync by registering {
        inputs.files(configurations.getByName("protos"))
        outputs.dir(buildDir.resolve("proto"))

        doFirst {
            buildDir.resolve("proto").mkdir()
            sync {
                from(configurations.getByName("protos"))
                into(buildDir.resolve("proto"))
            }
        }
    }

    val downloadRust by registering(SetupRust::class) {
        rustVersion.set("1.52.1")
        targets.add("arm-unknown-linux-gnueabihf")
    }

    val build by registering(CargoTask::class) {
        group = "build"
        dependsOn(downloadRust, protoSync)

        // For up-to-date
        inputs.files("Cargo.lock", "Cargo.toml")
        inputs.dir(".cargo")
        inputs.dir("src")
        inputs.files(protoSync.get().outputs)
        outputs.file(buildDir.resolve("bin/arm/secure-gpio-daemon"))

        environment.put("VERSION", version.toString())
        args.addAll("build", "--release", "--target", "arm-unknown-linux-gnueabihf", "--locked")

        doLast {
            buildDir.resolve("bin/arm").mkdirs()
            copy {
                from(file("target/arm-unknown-linux-gnueabihf/release/secure-gpio-daemon"))
                into(buildDir.resolve("bin/arm"))
            }
        }
    }

    register<CargoTask>("buildDebug") {
        group = "build"
        dependsOn(downloadRust)

        // For up-to-date
        inputs.files("Cargo.lock", "Cargo.toml")
        inputs.dir(".cargo")
        inputs.dir("src")
        outputs.file(buildDir.resolve("bin/debug/arm/secure-gpio-daemon"))

        environment.put("VERSION", version.toString())
        args.addAll("build", "--release", "--target", "arm-unknown-linux-gnueabihf", "--locked")

        doLast {
            buildDir.resolve("bin/debug/arm").mkdirs()
            copy {
                from(file("target/arm-unknown-linux-gnueabihf/debug/secure-gpio-daemon"))
                into(buildDir.resolve("bin/debug/arm"))
            }
        }
    }

    defaultTasks.add(build.name)
}
