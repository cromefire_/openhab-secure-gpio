use super::Adc;
use async_trait::async_trait;
use rppal::spi::{Bus, Mode, SlaveSelect, Spi};
use std::collections::HashMap;
use std::error::Error;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

/// Underlying implementation of the SPI protocol
struct Mcp3000 {
    spi: Arc<Mutex<Spi>>,
    callbacks: Arc<Mutex<HashMap<u8, Vec<Box<dyn FnMut(u16) + Send>>>>>,
}

impl Mcp3000 {
    fn new(bus: u8, chip_select: u8) -> Mcp3000 {
        let bus = match bus {
            0 => Bus::Spi0,
            1 => Bus::Spi1,
            2 => Bus::Spi2,
            3 => Bus::Spi3,
            4 => Bus::Spi4,
            5 => Bus::Spi5,
            6 => Bus::Spi6,
            _ => {
                panic!("Invalid bus: {}", bus);
            }
        };
        let chip_select = match chip_select {
            0 => SlaveSelect::Ss0,
            1 => SlaveSelect::Ss1,
            2 => SlaveSelect::Ss2,
            3 => SlaveSelect::Ss3,
            4 => SlaveSelect::Ss4,
            5 => SlaveSelect::Ss5,
            6 => SlaveSelect::Ss6,
            7 => SlaveSelect::Ss7,
            8 => SlaveSelect::Ss8,
            9 => SlaveSelect::Ss9,
            10 => SlaveSelect::Ss10,
            11 => SlaveSelect::Ss11,
            12 => SlaveSelect::Ss12,
            13 => SlaveSelect::Ss13,
            14 => SlaveSelect::Ss14,
            15 => SlaveSelect::Ss15,
            _ => {
                panic!("Invalid chip select: {}", chip_select);
            }
        };
        let spi = Spi::new(bus, chip_select, 1_200_000, Mode::Mode0).unwrap();
        Mcp3000 {
            spi: Arc::new(Mutex::new(spi)),
            callbacks: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    async fn listen_analog(
        &mut self,
        pin: u8,
        shift: u8,
        callback: Box<dyn FnMut(u16) + Send>,
    ) -> Result<(), Box<dyn Error>> {
        let mut map = self.callbacks.lock().unwrap();
        if let Some(emitter) = map.get_mut(&pin) {
            emitter.push(callback);
            return Ok(());
        }
        map.insert(pin, vec![callback]);

        let spi_ref = self.spi.clone();
        let callback_map = self.callbacks.clone();
        thread::spawn(move || loop {
            thread::sleep(Duration::from_micros(100));
            let mut callback_map = callback_map.lock().unwrap();
            let callbacks = callback_map.get_mut(&pin);
            if callbacks.is_none() {
                break;
            }
            let callbacks = callbacks.unwrap();
            let spi = spi_ref.lock().unwrap();
            let msg = 0b11000000 + (pin << shift);
            let mut res = [0; 2];
            let size = spi.transfer(&mut res, &[msg, 0b00000000]).unwrap();
            assert_eq!(size, 2);
            let value = ((res[0] as u16) << 8) + res[1] as u16;
            for callback in callbacks.iter_mut() {
                callback(value);
            }
        });
        Ok(())
    }
}

pub struct Mcp3002 {
    com: Mcp3000,
}

impl Mcp3002 {
    pub fn new(bus: u8, chip_select: u8) -> Mcp3002 {
        log::debug!("New MCP3002 IC connection: SPI{} CE{}", bus, chip_select);
        Mcp3002 {
            com: Mcp3000::new(bus, chip_select),
        }
    }
}

#[async_trait]
impl Adc for Mcp3002 {
    async fn listen_analog(&mut self, pin: u8, callback: Box<dyn FnMut(u16) + Send>) -> Result<(), Box<dyn Error>> {
        assert!(pin <= 1);
        return self.com.listen_analog(pin, 5, callback).await;
    }

    fn max_pin(&self) -> u8 {
        1
    }

    fn max_value(&self) -> u16 {
        // 10bit
        1023
    }
}

pub struct Mcp3004 {
    com: Mcp3000,
}

impl Mcp3004 {
    pub fn new(bus: u8, chip_select: u8) -> Mcp3004 {
        log::debug!("New MCP3004 IC connection: SPI{} CE{}", bus, chip_select);
        Mcp3004 {
            com: Mcp3000::new(bus, chip_select),
        }
    }
}

#[async_trait]
impl Adc for Mcp3004 {
    async fn listen_analog(&mut self, pin: u8, callback: Box<dyn FnMut(u16) + Send>) -> Result<(), Box<dyn Error>> {
        assert!(pin <= 3);
        return self.com.listen_analog(pin, 3, callback).await;
    }

    fn max_pin(&self) -> u8 {
        3
    }

    fn max_value(&self) -> u16 {
        // 10bit
        1023
    }
}

pub struct Mcp3008 {
    com: Mcp3000,
}

impl Mcp3008 {
    pub fn new(bus: u8, chip_select: u8) -> Mcp3008 {
        log::debug!("New MCP3008 IC connection: SPI{} CE{}", bus, chip_select);
        Mcp3008 {
            com: Mcp3000::new(bus, chip_select),
        }
    }
}

#[async_trait]
impl Adc for Mcp3008 {
    async fn listen_analog(&mut self, pin: u8, callback: Box<dyn FnMut(u16) + Send>) -> Result<(), Box<dyn Error>> {
        assert!(pin <= 7);
        return self.com.listen_analog(pin, 3, callback).await;
    }

    fn max_pin(&self) -> u8 {
        7
    }

    fn max_value(&self) -> u16 {
        // 10bit
        1023
    }
}

pub struct Mcp3208 {
    com: Mcp3000,
}

impl Mcp3208 {
    pub fn new(bus: u8, chip_select: u8) -> Mcp3208 {
        log::debug!("New MCP3208 IC connection: SPI{} CE{}", bus, chip_select);
        Mcp3208 {
            com: Mcp3000::new(bus, chip_select),
        }
    }
}

#[async_trait]
impl Adc for Mcp3208 {
    async fn listen_analog(&mut self, pin: u8, callback: Box<dyn FnMut(u16) + Send>) -> Result<(), Box<dyn Error>> {
        assert!(pin <= 7);
        return self.com.listen_analog(pin, 3, callback).await;
    }

    fn max_pin(&self) -> u8 {
        7
    }

    fn max_value(&self) -> u16 {
        // 12bit
        4095
    }
}
