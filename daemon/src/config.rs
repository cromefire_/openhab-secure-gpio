use serde::Deserialize;
use std::net::IpAddr;

#[derive(Debug, PartialEq, Deserialize)]
pub struct Config {
    pub port: Option<u16>,
    pub ip: Option<IpAddr>,
    pub pwm_modules: Option<Vec<PwmModule>>,
    pub analog_inputs: Option<Vec<AnalogInputs>>,
}

#[derive(Debug, PartialEq, Deserialize)]
#[serde(tag = "type")]
pub enum PwmModule {
    PiGpio,
    PCA9685 { address: u8 },
}

#[derive(Debug, PartialEq, Deserialize)]
#[serde(tag = "type")]
pub enum AnalogInputs {
    MCP3002 { bus: u8, chip_select: u8 },
    MCP3004 { bus: u8, chip_select: u8 },
    MCP3008 { bus: u8, chip_select: u8 },
    MCP3208 { bus: u8, chip_select: u8 },
}
