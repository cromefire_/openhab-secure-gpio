use crate::adc::mcp3000::{Mcp3002, Mcp3004, Mcp3008, Mcp3208};
use crate::adc::Adc;
use crate::certs::generate_certs;
use crate::config::{AnalogInputs, Config, PwmModule};
use crate::pb::secure_gpio_server::SecureGpioServer;
use crate::pwm::pca9685::PCA9685Pwm;
use crate::pwm::pigpio::PiGpioPwm;
use crate::pwm::Pwm;
use crate::server::SecureGpioService;
use crate::tls::load_tls_config;
use crate::validation::{validate_pca9685_address, validate_pigpio, validate_spi_parameters};
use crate::version::VERSION;
use serde::Serialize;
use std::fs::File;
use std::io::{BufReader, ErrorKind};
use std::net::SocketAddr;
use std::process::exit;
use std::time::Duration;
use std::{env, thread};
use tonic::transport::Server;

mod adc;
mod certs;
mod config;
mod pwm;
mod server;
mod tls;
mod validation;
mod version;

mod pb {
    tonic::include_proto!("proto");
}

#[derive(Serialize)]
struct Cmd {
    cmd: u32,
    p1: u32,
    p2: u32,
    p3: u32,
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    log::info!("Version {}", VERSION);

    let args: Vec<String> = env::args().collect();
    if args.contains(&"--help".to_string()) {
        eprintln!("secure-gpio-daemon [generate-certs <ip/hostname of daemon>]");
        exit(0);
    }
    if let Some(cmd) = args.get(1) {
        match cmd.as_str() {
            "generate-certs" => {
                let daemon_host = args.get(2).unwrap_or_else(|| {
                    log::error!("Missing ip/hostname of daemon");
                    exit(1);
                });
                generate_certs(daemon_host).unwrap();
            }
            &_ => {
                log::error!("Invalid subcommand `{}'", cmd);
                exit(1)
            }
        }

        exit(0);
    }
    let f = File::open("config.yaml");
    if let Err(err) = f {
        if err.kind() == ErrorKind::NotFound {
            log::error!("Config file \"config.yaml\" not found.");
            exit(1);
        }
        log::error!("{}", err);
        exit(1)
    }
    let config_result: Result<Config, _> = serde_yaml::from_reader(BufReader::new(f.unwrap()));
    let config = if let Err(err) = config_result {
        if err.to_string() == "Error: EndOfStream" || err.to_string() == "EOF while parsing a value"
        {
            Config {
                ip: None,
                port: None,
                pwm_modules: None,
                analog_inputs: None,
            }
        } else {
            log::error!("{}", err);
            exit(1)
        }
    } else {
        config_result.unwrap()
    };

    let mut pwm_modules: Vec<Box<dyn Pwm + Send + Sync>> = vec![];
    for pm in config.pwm_modules.unwrap_or_default() {
        match pm {
            PwmModule::PiGpio => {
                validate_pigpio();
                pwm_modules.push(Box::new(PiGpioPwm::new()));
            }
            PwmModule::PCA9685 { address } => {
                validate_pca9685_address(address);
                pwm_modules.push(Box::new(PCA9685Pwm::new(address, None, None).unwrap()));
            }
        }
    }

    let mut analog_inputs: Vec<Box<dyn Adc + Send + Sync>> = vec![];
    for ai in config.analog_inputs.unwrap_or_default() {
        match ai {
            AnalogInputs::MCP3002 { bus, chip_select } => {
                validate_spi_parameters(bus, chip_select);
                analog_inputs.push(Box::new(Mcp3002::new(bus, chip_select)));
            }
            AnalogInputs::MCP3004 { bus, chip_select } => {
                validate_spi_parameters(bus, chip_select);
                analog_inputs.push(Box::new(Mcp3004::new(bus, chip_select)));
            }
            AnalogInputs::MCP3008 { bus, chip_select } => {
                validate_spi_parameters(bus, chip_select);
                analog_inputs.push(Box::new(Mcp3008::new(bus, chip_select)));
            }
            AnalogInputs::MCP3208 { bus, chip_select } => {
                validate_spi_parameters(bus, chip_select);
                analog_inputs.push(Box::new(Mcp3208::new(bus, chip_select)));
            }
        }
    }

    let addr: SocketAddr = SocketAddr::new(
        config.ip.unwrap_or_else(|| "0.0.0.0".parse().unwrap()),
        config.port.unwrap_or(8443),
    );
    let gpio_srv = SecureGpioService::new(pwm_modules, analog_inputs);

    let server_tls_config = load_tls_config();

    log::info!("Address: {}", addr);
    Server::builder()
        .tls_config(server_tls_config)
        .unwrap()
        .add_service(SecureGpioServer::new(gpio_srv))
        .serve(addr)
        .await
        .unwrap();
}
