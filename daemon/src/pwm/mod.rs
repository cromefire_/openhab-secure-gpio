use std::error::Error;
use async_trait::async_trait;

pub mod pca9685;
pub mod pigpio;

#[async_trait]
pub trait Pwm {
    async fn set_pwm(&mut self, pin: u8, value: u16) -> Result<(), Box<dyn Error>>;

    fn max_value(&self) -> u16;

    fn max_pin(&self) -> u8;
}
