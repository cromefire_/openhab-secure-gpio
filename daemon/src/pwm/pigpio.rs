use std::io::{Error, Write};
use super::Pwm;
use async_trait::async_trait;

pub struct PiGpioPwm;

impl PiGpioPwm {
    pub fn new() -> PiGpioPwm {
        PiGpioPwm {}
    }
}

#[async_trait]
impl Pwm for PiGpioPwm {
    async fn set_pwm(&mut self, pin: u8, value: u16) -> Result<(), Box<dyn std::error::Error>> {
        if value > 255 {
            panic!("Value may not exceed 255")
        }
        gpio_pwm(pin, value as u8).await?;
        Ok(())
    }

    fn max_value(&self) -> u16 {
        255
    }

    fn max_pin(&self) -> u8 {
        27
    }
}

pub async fn gpio_pwm(pin: u8, value: u8) -> Result<(), Error> {
    let mut file = unix_named_pipe::open_write("/dev/pigpio")?;
    file.write_all(format!("p {} {}\n", pin, value).as_bytes())?;
    Ok(())
}
