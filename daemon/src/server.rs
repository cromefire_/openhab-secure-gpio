use crate::adc::Adc;
use crate::pb::secure_gpio_server::SecureGpio;
use crate::pb::{
    write_digital_pin, write_pwm_pin, DaemonInfo, PinAnalogValue, PinDigitalValue,
    ReadPinAnalogConfiguration, ReadPinAnalogThresholdConfiguration, ReadPinConfiguration,
    WriteDigitalPin, WritePwmPin,
};
use crate::pwm::Pwm;
use crate::version::VERSION;
use async_std::sync::{Arc, Mutex, RwLock};
use async_std::task;
use futures::executor;
use log::debug;
use rppal::gpio::{Gpio, Level, OutputPin, Trigger};
use rppal::system::DeviceInfo;
use std::ops::DerefMut;
use std::option::Option::Some;
use tokio::sync::mpsc;
use tokio_stream::wrappers::ReceiverStream;
use tonic::{Code, Request, Response, Status, Streaming};

pub struct SecureGpioService {
    pwm: Arc<Mutex<Vec<Box<dyn Pwm + Send + Sync>>>>,
    adc: Arc<Mutex<Vec<Box<dyn Adc + Send + Sync>>>>,
    info: DeviceInfo,
}

impl SecureGpioService {
    pub fn new(
        pwm: Vec<Box<dyn Pwm + Send + Sync>>,
        adc: Vec<Box<dyn Adc + Send + Sync>>,
    ) -> SecureGpioService {
        SecureGpioService {
            pwm: Arc::new(Mutex::new(pwm)),
            adc: Arc::new(Mutex::new(adc)),
            info: DeviceInfo::new().unwrap(),
        }
    }
}

#[tonic::async_trait]
impl SecureGpio for SecureGpioService {
    async fn write_pwm_pin(
        &self,
        request: Request<Streaming<WritePwmPin>>,
    ) -> Result<Response<()>, Status> {
        let mut stream = request.into_inner();
        let mut module: usize = 0;
        let mut pin: u8 = 0;
        while let Some(next_message) = stream.message().await? {
            if let Some(tpe) = next_message.r#type {
                match tpe {
                    write_pwm_pin::Type::PinSelect(p) => {
                        let pwms = self.pwm.lock().await;
                        module = p.pwm_module as usize;
                        if module > pwms.len() {
                            return Err(Status::new(Code::FailedPrecondition, "Module not found."));
                        }
                        pin = p.pin as u8;
                        let pwm = pwms.get(module - 1).unwrap();
                        if pin - 1 > pwm.max_pin() {
                            return Err(Status::new(
                                Code::FailedPrecondition,
                                format!("Valid pin range is 1 - {}.", pwm.max_pin() + 1),
                            ));
                        }
                    }
                    write_pwm_pin::Type::Value(v) => {
                        println!("Updating pin {}->{}: {}", module, pin - 1, v);
                        if pin == 0 {
                            return Err(Status::new(Code::FailedPrecondition, "Pin is unset."));
                        }
                        if module == 0 {
                            return Err(Status::new(Code::FailedPrecondition, "Module is unset."));
                        }
                        let mut pwms = self.pwm.lock().await;
                        let pwm = pwms.get_mut(module - 1).unwrap();
                        if v > pwm.max_value() as u32 {
                            return Err(Status::new(
                                Code::FailedPrecondition,
                                format!("Maximum value is {}.", pwm.max_value()),
                            ));
                        }
                        println!("Updating pin {}->{}: {}", module, pin - 1, v);
                        if let Err(e) = pwm.set_pwm(pin - 1, v as u16).await {
                            println!("Failed to update pin {}: {}", pin - 1, e);
                        }
                    }
                }
            }
        }

        Ok(Response::new(()))
    }

    async fn write_digital_pin(
        &self,
        request: Request<Streaming<WriteDigitalPin>>,
    ) -> Result<Response<()>, Status> {
        let mut stream = request.into_inner();
        let gpio = Gpio::new().unwrap(); // TODO
        let pin: Arc<RwLock<Option<OutputPin>>> = Arc::new(RwLock::new(None));
        while let Some(next_message) = stream.message().await? {
            if let Some(tpe) = next_message.r#type {
                match tpe {
                    write_digital_pin::Type::Pin(p) => {
                        let mut pin_ref = pin.write().await;
                        // TODO
                        *pin_ref = Some(gpio.get(p as u8).unwrap().into_output());
                    }
                    write_digital_pin::Type::Value(v) => {
                        let mut guard = pin.write().await;
                        if let Some(pin) = guard.as_mut() {
                            if v {
                                pin.set_high()
                            } else {
                                pin.set_low()
                            }
                        } else {
                            return Err(Status::new(Code::FailedPrecondition, "Pin is unset."));
                        }
                    }
                }
            }
        }
        Ok(Response::new(()))
    }

    type readDigitalPinStream = ReceiverStream<Result<PinDigitalValue, Status>>;

    async fn read_digital_pin(
        &self,
        request: Request<ReadPinConfiguration>,
    ) -> Result<Response<Self::readDigitalPinStream>, Status> {
        let gpio = Gpio::new().unwrap(); // TODO
        let pin_num = request.into_inner().pin as u8;
        let mut pin = gpio.get(pin_num).unwrap().into_input_pulldown(); // TODO

        let (tx, rx) = mpsc::channel(1);

        let mut previous = pin.read();
        tx.send(Ok(PinDigitalValue {
            value: match previous {
                Level::Low => false,
                Level::High => true,
            },
        }))
        .await
        .unwrap(); // TODO

        let tx2 = tx.clone();

        println!("set interrupt for {}", pin_num);
        pin.set_async_interrupt(Trigger::Both, move |level| {
            if level == previous {
                return;
            }
            println!("got interrupt: {}", level);
            match level {
                Level::Low => {
                    task::block_on(async {
                        tx.send(Ok(PinDigitalValue { value: false })).await.unwrap();
                        // TODO
                    });
                }
                Level::High => {
                    task::block_on(async {
                        tx.send(Ok(PinDigitalValue { value: true })).await.unwrap();
                        // TODO
                    });
                }
            }
            previous = level;
        })
        .unwrap(); // TODO

        tokio::spawn(async move {
            tx2.closed().await;
            let _ = pin.clear_async_interrupt();
        });

        Ok(Response::new(ReceiverStream::new(rx)))
    }

    type readAnalogPinStream = ReceiverStream<Result<PinAnalogValue, Status>>;

    async fn read_analog_pin(
        &self,
        request: Request<ReadPinAnalogConfiguration>,
    ) -> Result<Response<Self::readAnalogPinStream>, Status> {
        let config: ReadPinAnalogConfiguration = request.into_inner();
        let mut adcs = self.adc.lock().await;
        let module: u32 = config.module;
        let pin: u32 = config.pin;
        if module == 0 || module > adcs.len() as u32 {
            return Err(Status::failed_precondition("Module not found."));
        }
        let adc = adcs.get_mut((config.module - 1) as usize).unwrap();
        if pin == 0 || pin > adc.max_pin() as u32 {
            return Err(Status::failed_precondition(format!(
                "Valid pin rage 1 - {}.",
                adc.max_pin() + 1
            )));
        }
        if config.jitter >= adc.max_value() as u32 {
            return Err(Status::failed_precondition(format!(
                "Valid jitter rage 0 - {}.",
                adc.max_value() - 1
            )));
        }
        let jitter = config.jitter as u16;
        let (tx, rx) = mpsc::channel(1);
        let last_value = Arc::new(Mutex::new(None as Option<u16>));
        let res = adc
            .listen_analog(
                (pin - 1) as u8,
                Box::new(move |value| {
                    let txc = tx.clone();
                    let last_value = last_value.clone();
                    executor::block_on(async move {
                        let mut last_value = last_value.lock().await;
                        let should_skip = last_value
                            .and_then(|v| {
                                let over_low_jitter_threshold = value >= v.saturating_sub(jitter);
                                let under_high_jitter_threshold = value <= v.saturating_add(jitter);
                                Some(over_low_jitter_threshold && under_high_jitter_threshold)
                            })
                            .unwrap_or(false);
                        if should_skip {
                            return;
                        }
                        log::debug!("Sending value for analog in {}.{}: {}", module - 1, pin - 1, value);
                        let res = txc
                            .send(Ok(PinAnalogValue {
                                value: value as u32,
                            }))
                            .await;
                        if let Err(_e) = res {
                            // TODO: Unsubscribe
                        } else {
                            *last_value.deref_mut() = Some(value);
                        }
                    });
                }),
            )
            .await;
        if let Err(e) = res {
            return Err(Status::internal(e.to_string()));
        }
        Ok(Response::new(ReceiverStream::new(rx)))
    }

    type readAnalogThresholdPinStream = ReceiverStream<Result<PinDigitalValue, Status>>;

    async fn read_analog_threshold_pin(
        &self,
        request: Request<ReadPinAnalogThresholdConfiguration>,
    ) -> Result<Response<Self::readAnalogThresholdPinStream>, Status> {
        let config: ReadPinAnalogThresholdConfiguration = request.into_inner();
        let mut adcs = self.adc.lock().await;
        let module: u32 = config.module;
        let pin: u32 = config.module;
        if module == 0 || module > adcs.len() as u32 {
            return Err(Status::failed_precondition("Module not found."));
        }
        let adc = adcs.get_mut((module - 1) as usize).unwrap();
        if pin == 0 || pin > adc.max_pin() as u32 {
            return Err(Status::failed_precondition(format!(
                "Valid pin rage 1 - {}.",
                adc.max_pin() + 1
            )));
        }
        if config.threshold_on >= adc.max_value() as u32 {
            return Err(Status::failed_precondition(format!(
                "Valid threshold_on rage 0 - {}.",
                adc.max_value() - 1
            )));
        }
        if config.threshold_off >= adc.max_value() as u32 {
            return Err(Status::failed_precondition(format!(
                "Valid threshold_off rage 0 - {}.",
                adc.max_value() - 1
            )));
        }
        if config.threshold_off > config.threshold_on {
            return Err(Status::failed_precondition(format!(
                "Valid threshold_off larger than threshold_on - {}.",
                adc.max_value() - 1
            )));
        }
        let threshold_on: u16 = config.threshold_on as u16;
        let threshold_off: u16 = config.threshold_off as u16;
        let (tx, rx) = mpsc::channel(1);
        let last_value = Arc::new(Mutex::new(None as Option<bool>));
        let res = adc
            .listen_analog(
                (config.pin - 1) as u8,
                Box::new(move |value| {
                    let txc = tx.clone();
                    let last_value = last_value.clone();
                    executor::block_on(async move {
                        let value = if value < threshold_off {
                            false
                        } else if value > threshold_on {
                            true
                        } else {
                            return;
                        };
                        let mut last_value = last_value.lock().await;
                        if last_value.and_then(|v| Some(v == value)).unwrap_or(false) {
                            return;
                        }
                        log::debug!("Sending value for analog threshold in {}.{}: {}", module - 1, pin - 1, value);
                        let res = txc.send(Ok(PinDigitalValue { value })).await;
                        if let Err(_e) = res {
                            // TODO: Unsubscribe
                        } else {
                            *last_value.deref_mut() = Some(value);
                        }
                    });
                }),
            )
            .await;
        if let Err(e) = res {
            return Err(Status::internal(e.to_string()));
        }
        Ok(Response::new(ReceiverStream::new(rx)))
    }

    async fn get_info(&self, _request: Request<()>) -> Result<Response<DaemonInfo>, Status> {
        debug!("Sending service info...");
        Ok(Response::new(DaemonInfo {
            version: VERSION.to_string(),
            hardware_model: self.info.model().to_string(),
            hostname: hostname::get().unwrap().into_string().unwrap(),
        }))
    }
}
