use rustls::ciphersuite::TLS13_AES_256_GCM_SHA384;
use rustls::internal::pemfile;
use rustls::{AllowAnyAuthenticatedClient, ProtocolVersion, RootCertStore, ServerConfig};
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::process::exit;
use tonic::transport::ServerTlsConfig;

pub(crate) fn load_tls_config() -> ServerTlsConfig {
    // Validate that files are in place
    let client_ca_path = Path::new("client-ca.pem");
    let server_cert_path = Path::new("server-cert.pem");
    let server_key_path = Path::new("server-key.pem");
    if !(client_ca_path.exists() && server_cert_path.exists() && server_key_path.exists()) {
        eprintln!("Required certificates not available. Generate using `generate-certs`");
        exit(1);
    }

    // Client auth
    let client_ca_pem = File::open("client-ca.pem").unwrap();
    let mut client_root_cert_store = RootCertStore::empty();
    client_root_cert_store
        .add_pem_file(&mut BufReader::new(client_ca_pem))
        .unwrap();
    let client_auth = AllowAnyAuthenticatedClient::new(client_root_cert_store);

    let mut tls = ServerConfig::with_ciphersuites(client_auth, &[&TLS13_AES_256_GCM_SHA384]);
    tls.versions = vec![ProtocolVersion::TLSv1_3];

    // Server cert
    let cert_file = File::open("server-cert.pem").unwrap();
    let certs = pemfile::certs(&mut BufReader::new(&cert_file)).unwrap();
    let key_file = File::open("server-key.pem").unwrap();
    let mut key = pemfile::pkcs8_private_keys(&mut BufReader::new(key_file)).unwrap();
    tls.set_single_cert(certs, key.remove(0)).unwrap();

    // ALPN
    tls.set_protocols(&["h2".as_bytes().to_vec()]);

    let mut server_tls_config = ServerTlsConfig::new();
    server_tls_config.rustls_server_config(tls);
    server_tls_config
}
