use std::path::Path;

pub fn validate_pigpio() {
    if Path::new(&"/dev/pigpio").exists() {
        return;
    }
    panic!("The pigpio device (/dev/pigpio) is not available");
}

pub fn validate_pca9685_address(address: u8) {
    if address >= 64 {
        panic!("The maximum I²C address for the PCA9685 is 63");
    }
}

pub fn validate_spi_parameters(bus: u8, chip_select: u8) {
    let path = format!("/dev/spidev{}.{}", bus, chip_select);
    if Path::new(&path).exists() {
        return;
    }
    panic!("SPI device {}.{} not found", bus, chip_select);
}
