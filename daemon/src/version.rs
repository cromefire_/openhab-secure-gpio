pub const VERSION: &str = match option_env!("VERSION") {
    None => "unknown",
    Some(s) => s,
};
