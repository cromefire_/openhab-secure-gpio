import com.google.protobuf.gradle.*
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val grpcVersion: String by project
val protobufVersion: String by project
val grpcKotlinVersion: String by project

plugins {
    kotlin("jvm")
    id("com.google.protobuf")
    idea
}

configurations {
    create("protos") {
        isCanBeConsumed = true
        isCanBeResolved = false
    }
}

artifacts {
    sourceSets.main.get().proto.srcDirs.forEach {
        add("protos", it)
    }
}

java {
    targetCompatibility = JavaVersion.VERSION_11
    sourceCompatibility = JavaVersion.VERSION_11
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:${rootProject.ext["protobufVersion"]}"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:${rootProject.ext["grpcVersion"]}"
        }
        id("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:${rootProject.ext["grpcKotlinVersion"]}:jdk7@jar"
        }
    }
    generateProtoTasks {
        all().forEach {
            it.plugins {
                id("grpc")
                id("grpckt")
            }
            it.builtins {
                named("java") {
                    option("annotate_code")
                }
                id("kotlin")
            }
        }
    }
}

repositories {
    mavenCentral()
}

dependencies {
    @Suppress("GradlePackageUpdate")
    api("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.2")

    api("io.grpc:grpc-protobuf:$grpcVersion")
    api("com.google.protobuf:protobuf-java-util:$protobufVersion")
    api("io.grpc:grpc-stub:$grpcVersion")
    api("io.grpc:grpc-kotlin-stub:$grpcKotlinVersion")
    api("com.google.protobuf:protobuf-kotlin:$protobufVersion")
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = "11"
            freeCompilerArgs = listOf("-Xopt-in=kotlin.RequiresOptIn")
        }
    }

    afterEvaluate {
        named<GenerateProtoTask>("generateProto") {
            doFirst {
                delete(outputBaseDir)
            }
        }
    }
}
